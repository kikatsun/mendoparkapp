package com.example.mendoparkapp;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;

import com.example.mendoparkapp.model.Role;
import com.example.mendoparkapp.service.Preferencias;
import com.example.mendoparkapp.service.ServicioRest;
import com.example.mendoparkapp.ui.gallery.Consultar;
import com.example.mendoparkapp.ui.gallery.GalleryViewModel;
import com.example.mendoparkapp.ui.gallery.GeoPoint;
import com.example.mendoparkapp.ui.gallery.ParkingAreaDto;
import com.example.mendoparkapp.ui.gallery.ParkingSpaceDto;
import com.example.mendoparkapp.ui.gallery.PolygonPointsDto;
import com.example.mendoparkapp.ui.gallery.SensorStateDto;
import com.example.mendoparkapp.ui.share.ShareFragment;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.MutableLiveData;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback{

    public AppBarConfiguration mAppBarConfiguration;
    private MutableLiveData<String> mText;
    private GoogleMap mMap;
    private LinearLayout menu2;
    private LinearLayout menu2_buttons;
    private TextView menu2_title;

    private ParkingAreaDto parkingAreaReserva = null;
    private ParkingSpaceDto parkingSpaceReserva = null;

    private String roleUser = RoleUserType.COMMON;

    public class RoleUserType{
        public static final String COMMON = "common";
        public static final String ADMIN = "admin";
    }


    public static class Sensor{
        public static final String s1 = "cny70Calle";
        public static final String s2 = "cny70A1";
        public static final Double la1 = -32.89647075924577D;
        public static final Double lo1 = -68.85342585239256D;
        public static final Double la2 = -32.896386443868295D;
        public static final Double lo2 = -68.85340258507061D;
    }

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Preferencias.getInstance(getApplicationContext());
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ShareFragment fr=new ShareFragment();
//            fr.setArguments(bn);
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.nav_host_fragment,fr)
                        .addToBackStack(null)
                        .commit();
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
            }
        });
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        if(Preferencias.getInstanciaSinContexto().getRole() == Role.Type.USER.getValue()){
            navigationView.inflateMenu(R.menu.user_menu);
            fab.setVisibility(View.INVISIBLE);
        }else{
            navigationView.inflateMenu(R.menu.activity_main_drawer);
        }
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_search, R.id.nav_slideshow,
                R.id.nav_tools, R.id.nav_share, R.id.nav_send)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        menu2 = findViewById(R.id.menu2);
        menu2_buttons = findViewById(R.id.menu2_buttons);
        menu2_title = findViewById(R.id.menu2_title);
        clearMenu2();

    }

    public void cerrar(){
        Preferencias.getInstanciaSinContexto().cerrarSesion();
        Preferencias.getInstanciaSinContexto().role(0);
        Intent newForm = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(newForm);
        Toast.makeText(getApplicationContext(), "Sesión finalizada", Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        try{
            MenuItem menuItem = menu.getItem(0);
            if(menuItem != null){
                menuItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        cerrar();
                        return false;
                    }
                });
            }
        }catch (Exception e){

        }
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        ConsultarAreas consultarAreas = new ConsultarAreas();
        List<ParkingAreaDto> dtos = new ArrayList<>();
        try {
            dtos = consultarAreas.execute().get();
        }
        catch(Exception e){
            Toast.makeText(
                    getApplicationContext(),
                    "ERROR al buscar las áres de estacionamiento",
                    Toast.LENGTH_SHORT
            ).show();
        }
        if(dtos != null && dtos.size()>0){
            for (int i = 0; i < dtos.size() ; i++) {
                ParkingAreaDto dto = dtos.get(i);
                PolygonOptions options = new PolygonOptions();
                for (int j = 0; j < dto.getPolygonPoints().size() ; j++) {
                    PolygonPointsDto point = dto.getPolygonPoints().get(j);
                    options.add(new LatLng(point.getLatitude(), point.getLongitude()));
                }
                options.strokeColor(0xFF00AA00)
                        .fillColor(0x2200FFFF)
                        .strokeWidth(2);
                Polygon polygon = mMap.addPolygon(options);
            }
        }
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            public boolean onMarkerClick(Marker marker) {
                String sensorName = marker.getTitle();
                ConsultarEstadoSensor consultarEstadoSensor = new ConsultarEstadoSensor();
                SensorStateDto dto = null;
                try {
                    dto = consultarEstadoSensor.execute(sensorName).get();
                }catch(Exception ex){
                    Toast.makeText(
                            getApplicationContext(),
                            "ERROR al consultar estado de sensor",
                            Toast.LENGTH_SHORT
                    ).show();
                }
                if(dto != null) {
                    Toast.makeText(
                            getApplicationContext(),
                            "EL sensor: " + sensorName + " tiene el estado: " + dto.getState(),
                            Toast.LENGTH_LONG
                    ).show();
                }
                return true;
            }
        });

    }

//    public void buscar (View v){
//        Consultar c =new Consultar();
//        ConsultarLogin o = new ConsultarLogin();
//        GeoPoint point = null;
//
//        try {
//            point = o.execute(
//                    location.getText().toString()
//            ).get();
//        }
//        catch(Exception e){
//            Toast.makeText(
//                    getApplicationContext(),
//                    "ERROR",
//                    Toast.LENGTH_SHORT
//            ).show();
//        }
//
//        if(point == null){
//            Toast.makeText(
//                    getApplicationContext(),
//                    "Localización no encontrada",
//                    Toast.LENGTH_SHORT
//            ).show();
//        }else {
//
//            LatLng sydney = new LatLng(point.getLatitude(), point.getLongitude());
//
//            if (c.estaDentro(point)) {
////        if(true){
//                MarkerOptions mk = new MarkerOptions();
//
//                mMap.addMarker(new MarkerOptions().position(sydney).title("Punto Consultado"));
//                mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
//
//
//                LatLng s1 = new LatLng(Sensor.la1, Sensor.lo1);
//                mMap.addMarker(new MarkerOptions().position(s1).title(Sensor.s1)).setIcon(BitmapDescriptorFactory.fromResource(R.drawable.sensor));
//                mMap.moveCamera(CameraUpdateFactory.newLatLng(s1));
//                mMap.animateCamera(CameraUpdateFactory.zoomTo(17.0f));
//                LatLng s2 = new LatLng(Sensor.la2, Sensor.lo2);
//                mMap.addMarker(new MarkerOptions().position(s2).title(Sensor.s2)).setIcon(BitmapDescriptorFactory.fromResource(R.drawable.sensor));
//
//            } else {
//                LatLng aa = new LatLng(point.getLatitude(), point.getLongitude());
//                mMap.addMarker(new MarkerOptions().position(aa).title("Punto Consultado"));
//                mMap.moveCamera(CameraUpdateFactory.newLatLng(aa));
//                mMap.animateCamera(CameraUpdateFactory.zoomTo(17.0f));
//            }
//        }
//
//    }

    private class ConsultarLogin extends AsyncTask<String,Integer,GeoPoint> {

        GeoPoint result;

        @Override
        protected void onPreExecute(){

        }

        @Override
        protected void onProgressUpdate(Integer... values) {

        }

        @SuppressLint("WrongThread")
        @Override
        protected GeoPoint doInBackground(String... params) {

            /**
             * Convierte la información en DTOUsuario.
             */
            String location = (params[0]);
            String resultAsResult = null;
            Double longitude = null;
            Double latitude = null;
            HttpURLConnection conection = null;

            location = location.trim();
            location = location.replaceAll(" ","%20");
            String urlString = Consultar.headUrl + location + Consultar.tailUrl;
            StringBuilder resultado = new StringBuilder();
            try {
                // Crear un objeto de tipo URL
                URL url = new URL(urlString);
                conection = (HttpURLConnection) url.openConnection();

                conection.setRequestMethod("GET");
                conection.setDoInput(true);

                conection.setConnectTimeout(1000);
                conection.setReadTimeout(1000);

                conection.connect();


                /**
                 * Prepara la recepción de datos de la conexión mediante las clases InputStream
                 * y InputStreamReader.
                 */
                InputStream in = new BufferedInputStream(conection.getInputStream());
                InputStreamReader is = new InputStreamReader(in);

                /**
                 * Convierte los datos recibidos en un String y cierra la conexión
                 */
                StringBuilder sb = new StringBuilder();
                BufferedReader br = new BufferedReader(is);

                String read = br.readLine();

                while(read!=null){
                    sb.append(read);
                    read = br.readLine();
                }

                br.close();
                conection.disconnect();
                resultAsResult = sb.toString();
                if(resultAsResult != null && resultAsResult.length() > 2){
                    JSONArray responseBodyAsArray = null;
                    responseBodyAsArray = new JSONArray(new String(resultAsResult));
                    if(responseBodyAsArray.length() > 0){
                        JSONObject responseBodyAsJSON = new JSONObject(responseBodyAsArray.get(0).toString());
                        if(responseBodyAsJSON.has(Consultar.OpenStreetMap.LATITUDE) && responseBodyAsJSON.has(Consultar.OpenStreetMap.LONGITUDE)){
                            longitude = Double.parseDouble(responseBodyAsJSON.getString(Consultar.OpenStreetMap.LONGITUDE));
                            latitude = Double.parseDouble(responseBodyAsJSON.getString(Consultar.OpenStreetMap.LATITUDE));
                            result = new GeoPoint(latitude, longitude);
                        }
                    }
                }


            }catch (Exception e){
            }
            return result;
        }

        /**
         * Después de terminar de ejecutar la comunicación procesa la información recibida.
         * Convierte el String de restpuesta en un DTORespuesta, si es correcto lo direcciona
         * a la venta principal, sino muestra mensaje de error según el detalle del error.
         */
        @Override
        protected void onPostExecute(GeoPoint aVoid) {

        }
    }

    private class ConsultarAreas extends AsyncTask<String,Integer, List<ParkingAreaDto>> {

        List<ParkingAreaDto> result = new ArrayList<>();

        @Override
        protected void onPreExecute(){

        }

        @Override
        protected void onProgressUpdate(Integer... values) {

        }

        @SuppressLint("WrongThread")
        @Override
        protected List<ParkingAreaDto> doInBackground(String... params) {

            /**
             * Convierte la información en DTOUsuario.
             */
            String resultAsResult = null;
            HttpURLConnection conection = null;

            String urlString = ServicioRest.getInstancia().getUrl() + "ParkingArea/parkingAreas";
            StringBuilder resultado = new StringBuilder();
            try {
                // Crear un objeto de tipo URL
                URL url = new URL(urlString);
                conection = (HttpURLConnection) url.openConnection();

                conection.setRequestMethod("GET");
                conection.setDoInput(true);

                conection.setConnectTimeout(1000);
                conection.setReadTimeout(1000);

                conection.connect();


                /**
                 * Prepara la recepción de datos de la conexión mediante las clases InputStream
                 * y InputStreamReader.
                 */
                InputStream in = new BufferedInputStream(conection.getInputStream());
                InputStreamReader is = new InputStreamReader(in);

                /**
                 * Convierte los datos recibidos en un String y cierra la conexión
                 */
                StringBuilder sb = new StringBuilder();
                BufferedReader br = new BufferedReader(is);

                String read = br.readLine();

                while(read!=null){
                    sb.append(read);
                    read = br.readLine();
                }

                br.close();
                conection.disconnect();
                resultAsResult = sb.toString();
                if(resultAsResult != null && resultAsResult.length() > 2){
                    JSONArray responseBodyAsArray = null;
                    responseBodyAsArray = new JSONArray(new String(resultAsResult));
                    if(responseBodyAsArray.length() > 0){
                        for (int j = 0; j < responseBodyAsArray.length(); j++) {
                            JSONObject responseBodyAsJSON = new JSONObject(responseBodyAsArray.get(j).toString());
                            ParkingAreaDto parkingAreaDto = new ParkingAreaDto();
                            if(responseBodyAsJSON.has("number")){
                                parkingAreaDto.setNumber(responseBodyAsJSON.getLong("number"));
                            }
                            if(responseBodyAsJSON.has("name")){
                                parkingAreaDto.setName(responseBodyAsJSON.getString("name"));
                            }
                            if(responseBodyAsJSON.has("color")){
                                parkingAreaDto.setColor(responseBodyAsJSON.getString("color"));
                            }
                            if(responseBodyAsJSON.has("polygonPoints")){
                                JSONArray pointsArray = responseBodyAsJSON.getJSONArray("polygonPoints");
                                if(pointsArray!= null && pointsArray.length() > 0){
                                    List<PolygonPointsDto> polygonPointsDtos = new ArrayList<>();
                                    for (int i = 0; i < pointsArray.length(); i++) {
                                        JSONObject poinstAsJSON = new JSONObject(pointsArray.get(i).toString());
                                        PolygonPointsDto point = new PolygonPointsDto();
                                        if(poinstAsJSON.has("sequence")){
                                            point.setSequence(poinstAsJSON.getInt("sequence"));
                                        }
                                        if(poinstAsJSON.has("latitude")){
                                            point.setLatitude(poinstAsJSON.getDouble("latitude"));
                                        }
                                        if(poinstAsJSON.has("longitude")){
                                            point.setLongitude(poinstAsJSON.getDouble("longitude"));
                                        }
                                        polygonPointsDtos.add(point);
                                    }
                                    parkingAreaDto.setPolygonPoints(polygonPointsDtos);
                                }
                            }
                            result.add(parkingAreaDto);
                        }

                    }
                }


            }catch (Exception e){
            }
            return result;
        }

        /**
         * Después de terminar de ejecutar la comunicación procesa la información recibida.
         * Convierte el String de restpuesta en un DTORespuesta, si es correcto lo direcciona
         * a la venta principal, sino muestra mensaje de error según el detalle del error.
         */
        @Override
        protected void onPostExecute(List<ParkingAreaDto> aVoid) {

        }
    }

    private class ConsultarEstadoSensor extends AsyncTask<String,Integer, SensorStateDto> {

        SensorStateDto result;

        @Override
        protected void onPreExecute(){

        }

        @Override
        protected void onProgressUpdate(Integer... values) {

        }

        @SuppressLint("WrongThread")
        @Override
        protected SensorStateDto doInBackground(String... params) {

            /**
             * Convierte la información en DTOUsuario.
             */
            String resultAsResult = null;
            HttpURLConnection conection = null;

            String sensorName = params[0];

            String urlString = ServicioRest.getInstancia().getUrl() + "sensorstate/"+sensorName;
            StringBuilder resultado = new StringBuilder();
            try {
                // Crear un objeto de tipo URL
                URL url = new URL(urlString);
                conection = (HttpURLConnection) url.openConnection();

                conection.setRequestMethod("GET");
                conection.setDoInput(true);

                conection.setConnectTimeout(1000);
                conection.setReadTimeout(1000);

                conection.connect();


                /**
                 * Prepara la recepción de datos de la conexión mediante las clases InputStream
                 * y InputStreamReader.
                 */
                InputStream in = new BufferedInputStream(conection.getInputStream());
                InputStreamReader is = new InputStreamReader(in);

                /**
                 * Convierte los datos recibidos en un String y cierra la conexión
                 */
                StringBuilder sb = new StringBuilder();
                BufferedReader br = new BufferedReader(is);

                String read = br.readLine();

                while(read!=null){
                    sb.append(read);
                    read = br.readLine();
                }

                br.close();
                conection.disconnect();
                resultAsResult = sb.toString();
                if(resultAsResult != null && resultAsResult.length() > 2){
                    JSONObject res = new JSONObject(resultAsResult);
                    result = new SensorStateDto();
                    if(res.has("name")){
                        result.setName(res.getString("name"));
                    }
                    if(res.has("state")){
                        result.setState(res.getString("state"));
                    }
                }else{
                    result = null;
                }


            }catch (Exception e){
            }
            return result;
        }

        /**
         * Después de terminar de ejecutar la comunicación procesa la información recibida.
         * Convierte el String de restpuesta en un DTORespuesta, si es correcto lo direcciona
         * a la venta principal, sino muestra mensaje de error según el detalle del error.
         */
        @Override
        protected void onPostExecute(SensorStateDto aVoid) {

        }
    }

    public View buildTextView(String name){
        TextView result = new TextView(this);

        result.setText(name);
        result.setTextColor(Color.WHITE);
        result.setPadding(10,10,10,10);
        result.setTextSize(20);

        return result;
    }

    public void addPropertyToMenu2(String fieldName, String fieldValue){
        menu2.addView(buildTextView(fieldName));
        menu2.addView(buildTextView(fieldValue));
    }

    public void clearMenu2(){
        menu2.removeAllViews();
        menu2.addView(menu2_title);
    }

    public void addBUttonToMenu2(String name, View.OnClickListener action){
        addBUttonToMenu2(name, action, null);
    }

    public void addBUttonToMenu2(String name, View.OnClickListener action, String tag){
        Button button = new Button(this);
        button.setText(name);
        if(tag != null){
            button.setTag(tag);
        }
        button.setOnClickListener(action);
        menu2.addView(button);
    }

    public void showMenu2(){
        Toast.makeText(
            getApplicationContext(),
            "<---------------------------------------\n" +
                    "<---------------------------------------\n" +
                    "Deslice la pantalla hacia la izquierda para ver el detalle.",
            Toast.LENGTH_SHORT
        ).show();
    }

    public ParkingAreaDto getParkingAreaReserva() {
        return parkingAreaReserva;
    }

    public void setParkingAreaReserva(ParkingAreaDto parkingAreaReserva) {
        this.parkingAreaReserva = parkingAreaReserva;
    }

    public ParkingSpaceDto getParkingSpaceReserva() {
        return parkingSpaceReserva;
    }

    public void setParkingSpaceReserva(ParkingSpaceDto parkingSpaceReserva) {
        this.parkingSpaceReserva = parkingSpaceReserva;
    }

    @Override
    protected void onDestroy() {
        Preferencias.getInstanciaSinContexto().cerrarSesion();
        super.onDestroy();
    }
}
