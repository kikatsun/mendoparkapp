package com.example.mendoparkapp.dto;


import org.json.JSONException;
import org.json.JSONObject;

public class DTORespuesta {

    private boolean respuesta;
    private int tipo;
    private Object detalle;

    public boolean isRespuesta() {
        return respuesta;
    }
    public void setRespuesta(boolean respuesta) {
        this.respuesta = respuesta;
    }
    public int getTipo() {
        return tipo;
    }
    public void setTipo(int tipo) {
        this.tipo = tipo;
    }
    public Object getDetalle() {
        return detalle;
    }
    public void setDetalle(Object detalle) {
        this.detalle = detalle;
    }
    public DTORespuesta() {
        // TODO Auto-generated constructor stub
    }
    public DTORespuesta(boolean respuesta, int tipo, String detalle) {
        super();
        this.respuesta = respuesta;
        this.tipo = tipo;
        this.detalle = detalle;
    }

    public static class Factory{

        public static DTORespuesta getDTORespuestaFromStringJson(String jsonString) throws JSONException {
            DTORespuesta dtoRespuesta = new DTORespuesta();

            JSONObject jsonObject = new JSONObject(jsonString);
            dtoRespuesta.setTipo(jsonObject.getInt("tipo"));
            dtoRespuesta.setRespuesta(jsonObject.getBoolean("respuesta"));
            dtoRespuesta.setDetalle(jsonObject.getString("detalle"));

            return dtoRespuesta;
        }

    }
}
