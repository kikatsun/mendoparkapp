package com.example.mendoparkapp.dto.usuario;

import android.util.Log;

import com.example.mendoparkapp.dto.ConvertidorJSON;

import org.json.JSONException;
import org.json.JSONObject;

public class DTOUsuario implements ConvertidorJSON {

    private String userName;
    private String password;
    private String sessionId;
    private int role;

    public DTOUsuario() {
    }

    public DTOUsuario(String usuario, String contrasenia) {
        this.userName = usuario;
        this.password = contrasenia;
    }

    public String getUsuario() {
        return userName;
    }

    public void setUsuario(String usuario) {
        this.userName = usuario;
    }

    public String getContrasenia() {
        return password;
    }

    public void setContrasenia(String contrasenia) {
        this.password = contrasenia;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public JSONObject getJSON() throws JSONException {
        JSONObject json = new JSONObject();

        json.accumulate("userName",this.getUsuario());
        json.accumulate("password",this.getContrasenia());

        return json;
    }

    public static class Factory{

        public static DTOUsuario getDTORespuestaFromStringJson(String jsonString) {
            DTOUsuario result = null;

            try {
                JSONObject jsonObject = new JSONObject(jsonString);
                result = new DTOUsuario();
                if(jsonObject.has("userName")){
                    result.setUsuario(jsonObject.getString("userName"));
                }
                if(jsonObject.has("password")){
                    result.setContrasenia(jsonObject.getString("password"));
                }
                if(jsonObject.has("sessionId")){
                    result.setSessionId(jsonObject.getString("sessionId"));
                }
                if(jsonObject.has("role")){
                    result.setRole(jsonObject.getInt("role"));
                }
            } catch (JSONException e) {
                Log.d(DTOUsuario.class.getSimpleName(), e.getMessage());
            }
            return result;
        }
    }
}
