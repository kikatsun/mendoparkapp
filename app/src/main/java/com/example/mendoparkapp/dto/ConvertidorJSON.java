package com.example.mendoparkapp.dto;

import org.json.JSONException;
import org.json.JSONObject;

public interface ConvertidorJSON {

    public JSONObject getJSON() throws JSONException;
}
