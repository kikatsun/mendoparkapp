package com.example.mendoparkapp.ui.gallery;

public class ParkingSpaceDto {

    private String name;
    private Long fromParkingArea;
    private SensorDto sensor;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getFromParkingArea() {
        return fromParkingArea;
    }

    public void setFromParkingArea(Long fromParkingArea) {
        this.fromParkingArea = fromParkingArea;
    }

    public SensorDto getSensor() {
        return sensor;
    }

    public void setSensor(SensorDto sensor) {
        this.sensor = sensor;
    }


}
