package com.example.mendoparkapp.ui.send;

import com.example.mendoparkapp.dto.ConvertidorJSON;

import org.json.JSONException;
import org.json.JSONObject;

public class UserDto implements ConvertidorJSON {

    private String userName;
    private String password;
    private String email;
    private String documentNumber;
    private String name;
    private String lastName;
    private String phoneNumber;
    private String bithdate;
    private long documentType;
    private Integer roleId;


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getBithdate() {
        return bithdate;
    }

    public void setBithdate(String bithdate) {
        this.bithdate = bithdate;
    }

    public long getDocumentType() {
        return documentType;
    }

    public void setDocumentType(long documentType) {
        this.documentType = documentType;
    }


    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }


    @Override
    public JSONObject getJSON() throws JSONException {
        JSONObject json = new JSONObject();

        json.accumulate("userName",this.getUserName());
        json.accumulate("password",this.getPassword());
        json.accumulate("email",this.getEmail());
        json.accumulate("name",this.getName());
        json.accumulate("lastName",this.getLastName());
        json.accumulate("documentNumber",this.getDocumentNumber());

        return json;
    }
}
