package com.example.mendoparkapp.ui;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class GlobalViewModel extends ViewModel {

    private MutableLiveData<String> userSession;

    public GlobalViewModel(){
        userSession = new MutableLiveData<>();
        userSession.setValue(null);
    }

    public LiveData<String> getUserSession (){
        return userSession;
    }

}
