package com.example.mendoparkapp.ui.gallery;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class Consultar {

    public static final String headUrl = "https://nominatim.openstreetmap.org/search/?q=";
    public static final String tailUrl = "&format=json";

    public static final Double DISTANCIA = 0.009D;

    public class OpenStreetMap{
        public static final String HOST = "nominatim.openstreetmap.org";
        public static final String CONTEXT = "/search";
        public static final String FORMAT_PARAM = "format=";
        public static final String QUERY_PARAM = "q=";
        public static final String LATITUDE = "lat";
        public static final String LONGITUDE = "lon";
        public static final String FORMAT_PARAM_JSON_TYPE = "json";
        public static final int READ_TIMEOUT = 10000;
    }

    public static class UTN{
        public static final Double latitud = -32.89676153304492D;
        public static final Double longitud = -68.85343568761435D;
    }

    public GeoPoint getGeoPointFromLocation(String location){

        GeoPoint result = null;
        String resultAsResult = null;
        Double longitude = null;
        Double latitude = null;

        location = location.trim();
        location = location.replaceAll(" ","%20");
        String urlString = headUrl + location + tailUrl;
        StringBuilder resultado = new StringBuilder();
        try {
            // Crear un objeto de tipo URL
            URL url = new URL(urlString);

            // Abrir la conexión e indicar que será de tipo GET
            HttpURLConnection conexion = (HttpURLConnection) url.openConnection();
            conexion.setRequestMethod("GET");
            // Búferes para leer
            BufferedReader rd = new BufferedReader(new InputStreamReader(conexion.getInputStream()));
            String linea;
            // Mientras el BufferedReader se pueda leer, agregar contenido a resultado
            while ((linea = rd.readLine()) != null) {
                resultado.append(linea);
            }
            // Cerrar el BufferedReader
            rd.close();
            resultAsResult = resultado.toString();
        if(resultAsResult != null && resultAsResult.length() > 2){
            JSONArray responseBodyAsArray = null;
            responseBodyAsArray = new JSONArray(new String(resultAsResult));
            if(responseBodyAsArray.length() > 0){
                JSONObject responseBodyAsJSON = new JSONObject(responseBodyAsArray.get(0).toString());
                if(responseBodyAsJSON.has(OpenStreetMap.LATITUDE) && responseBodyAsJSON.has(OpenStreetMap.LONGITUDE)){
                    longitude = Double.parseDouble(responseBodyAsJSON.getString(OpenStreetMap.LONGITUDE));
                    latitude = Double.parseDouble(responseBodyAsJSON.getString(OpenStreetMap.LATITUDE));
                    result = new GeoPoint(latitude, longitude);
                }
            }
        }
        }catch (Exception e){
            return null;
        }
        return result;

    }

    public boolean estaDentro(GeoPoint point){
        boolean result = false;
        Double d1 = (point.getLatitude() - UTN.latitud);
        Double d2 = (point.getLongitude() - UTN.longitud);
        if(d1 < 0){
            d1 = d1 *-1;
        }
        if(d2 < 0){
            d2 = d2 *-1;
        }
        if(d1 < DISTANCIA && d2 < DISTANCIA){
            result = true;
        }

        return result;
    }

}
