package com.example.mendoparkapp.ui.slideshow;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import com.example.mendoparkapp.R;
import com.example.mendoparkapp.dto.DTORespuesta;
import com.example.mendoparkapp.model.ReservaDto;
import com.example.mendoparkapp.service.ReservationFactory;
import com.example.mendoparkapp.service.ServicioRest;
import com.example.mendoparkapp.ui.gallery.SensorStateDto;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.Calendar;

public class SlideshowFragment extends Fragment {

    private SlideshowViewModel slideshowViewModel;

    private TextView estacionamiento;
    private TextView nombrePlaza;
    private TextView precioPorHora;
    private TextView montoAnticipado;

    private Button reservarBtn;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        slideshowViewModel =
                ViewModelProviders.of(this).get(SlideshowViewModel.class);
        View root = inflater.inflate(R.layout.fragment_slideshow, container, false);

        estacionamiento = root.findViewById(R.id.nombreEstacionamiento);
        nombrePlaza = root.findViewById(R.id.nombrePlaza);
        precioPorHora = root.findViewById(R.id.precioPorHora);
        montoAnticipado = root.findViewById(R.id.montoAnticipo);

        reservarBtn = root.findViewById(R.id.reservarBtn);
        if(ReservationFactory.getInstance().isEnviado()){
            reservarBtn.setText("Consultar");
            reservarBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ConsultarParaReserva consultarParaReserva = new ConsultarParaReserva();
                    ReservaDto reserva = null;
                    try{
                        reserva = consultarParaReserva.execute().get();
                        ReservationFactory.getInstance().setReservaNueva(reserva);
                    }catch (Exception e){

                    }
                    if(reserva != null){

                    }
                }
            });
            return root;
        }else {
            reservarBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CambiarEstado cambiarEstado = new CambiarEstado();
                    try {
                        cambiarEstado.execute(ReservationFactory.getInstance().getReservaNueva()).get();
                        Toast.makeText(
                                getContext(),
                                "Reserva creada correctamente.",
                                Toast.LENGTH_SHORT
                        ).show();
                        Navigation.findNavController(getActivity(), R.id.nav_host_fragment).navigate(R.id.nav_home);
//
                    } catch (Exception e) {

                    }
                }
            });
        }

        ReservaDto nuevaReserva = ReservationFactory.getInstance().getReservaNueva();

        if(nuevaReserva == null){
            Toast.makeText(
                    getContext(),
                    "No hay reservas pendientes.",
                    Toast.LENGTH_SHORT
            ).show();
            Navigation.findNavController(getActivity(),R.id.nav_host_fragment).navigate(R.id.nav_home);
            return root;
        }

        ConsultarParaReserva consultarParaReserva = new ConsultarParaReserva();
        ReservaDto dto = null;
        try {
            dto = consultarParaReserva.execute(nuevaReserva.getParkingSpaceName()).get();
        } catch (Exception ex) {
            Toast.makeText(
                    getContext(),
                    "ERROR al consultar datos para la reserva",
                    Toast.LENGTH_SHORT
            ).show();
        }
        if(dto == null){
            return root;
        }
        dto.setEventoAccion(nuevaReserva.getEventoAccion());
        ReservationFactory.getInstance().setReservaNueva(dto);

        updateViewWithReserva(dto);

        return root;
    }

    public void updateViewWithReserva(ReservaDto reserva){
        estacionamiento.setText(reserva.getParkingAreaName());
        nombrePlaza.setText(reserva.getParkingSpaceName());
        precioPorHora.setText(String.valueOf(reserva.getPriceByHour()));
        montoAnticipado.setText(String.valueOf(reserva.getAnticipatedAmount()));
    }

    private class ConsultarParaReserva extends AsyncTask<String,Integer, ReservaDto> {

        ReservaDto result;

        @Override
        protected void onPreExecute(){

        }

        @Override
        protected void onProgressUpdate(Integer... values) {

        }

        @SuppressLint("WrongThread")
        @Override
        protected ReservaDto doInBackground(String... params) {

            /**
             * Convierte la información en DTOUsuario.
             */
            String resultAsResult = null;
            HttpURLConnection conection = null;

            String parkingName = params[0];

            String urlString = ServicioRest.getInstancia().getUrl() + "parking/"+parkingName;
            StringBuilder resultado = new StringBuilder();
            try {
                // Crear un objeto de tipo URL
                URL url = new URL(urlString);
                conection = (HttpURLConnection) url.openConnection();

                conection.setRequestMethod("GET");
                conection.setDoInput(true);

                conection.setConnectTimeout(1000);
                conection.setReadTimeout(1000);

                conection.connect();


                /**
                 * Prepara la recepción de datos de la conexión mediante las clases InputStream
                 * y InputStreamReader.
                 */
                InputStream in = new BufferedInputStream(conection.getInputStream());
                InputStreamReader is = new InputStreamReader(in);

                /**
                 * Convierte los datos recibidos en un String y cierra la conexión
                 */
                StringBuilder sb = new StringBuilder();
                BufferedReader br = new BufferedReader(is);

                String read = br.readLine();

                while(read!=null){
                    sb.append(read);
                    read = br.readLine();
                }

                br.close();
                conection.disconnect();
                resultAsResult = sb.toString();
                if(resultAsResult != null && resultAsResult.length() > 2 && !resultAsResult.equals("[]")){
                    JSONObject res = new JSONObject(resultAsResult);
                    result = new ReservaDto();
                    if(res.has("id")){
                        result.setId(res.getString("id"));
                    }
                    if(res.has("parkingAreaNumber")){
                        result.setParkingAreaNumber(res.getLong("parkingAreaNumber"));
                    }
                    if(res.has("parkingAreaName")){
                        result.setParkingAreaName(res.getString("parkingAreaName"));
                    }
                    if(res.has("idPlaza")){
                        result.setIdPlaza(res.getString("idPlaza"));
                    }
                    if(res.has("parkingSpaceName")){
                        result.setParkingSpaceName(res.getString("parkingSpaceName"));
                    }
                    if(res.has("priceByHour")){
                        result.setPriceByHour(Float.parseFloat(res.getString("priceByHour")));
                    }
                    if(res.has("anticipatedAmount")){
                        result.setAnticipatedAmount(Float.parseFloat(res.getString("anticipatedAmount")));
                    }
                }else{
                    result = null;
                }


            }catch (Exception e){
            }
            return result;
        }

        /**
         * Después de terminar de ejecutar la comunicación procesa la información recibida.
         * Convierte el String de restpuesta en un DTORespuesta, si es correcto lo direcciona
         * a la venta principal, sino muestra mensaje de error según el detalle del error.
         */
        @Override
        protected void onPostExecute(ReservaDto aVoid) {

        }
    }

    private class ConsultarEstadoSensor extends AsyncTask<String,Integer, SensorStateDto> {

        SensorStateDto result;

        @Override
        protected void onPreExecute(){

        }

        @Override
        protected void onProgressUpdate(Integer... values) {

        }

        @SuppressLint("WrongThread")
        @Override
        protected SensorStateDto doInBackground(String... params) {

            /**
             * Convierte la información en DTOUsuario.
             */
            String resultAsResult = null;
            HttpURLConnection conection = null;

            String sensorName = params[0];

            String urlString =ServicioRest.getInstancia().getUrl() + "sensorstate/"+sensorName;
            StringBuilder resultado = new StringBuilder();
            try {
                // Crear un objeto de tipo URL
                URL url = new URL(urlString);
                conection = (HttpURLConnection) url.openConnection();

                conection.setRequestMethod("GET");
                conection.setDoInput(true);

                conection.setConnectTimeout(1000);
                conection.setReadTimeout(1000);

                conection.connect();


                /**
                 * Prepara la recepción de datos de la conexión mediante las clases InputStream
                 * y InputStreamReader.
                 */
                InputStream in = new BufferedInputStream(conection.getInputStream());
                InputStreamReader is = new InputStreamReader(in);

                /**
                 * Convierte los datos recibidos en un String y cierra la conexión
                 */
                StringBuilder sb = new StringBuilder();
                BufferedReader br = new BufferedReader(is);

                String read = br.readLine();

                while(read!=null){
                    sb.append(read);
                    read = br.readLine();
                }

                br.close();
                conection.disconnect();
                resultAsResult = sb.toString();
                if(resultAsResult != null && resultAsResult.length() > 2){
                    JSONObject res = new JSONObject(resultAsResult);
                    result = new SensorStateDto();
                    if(res.has("name")){
                        result.setName(res.getString("name"));
                    }
                    if(res.has("state")){
                        result.setState(res.getString("state"));
                    }
                }else{
                    result = null;
                }


            }catch (Exception e){
            }
            return result;
        }

        /**
         * Después de terminar de ejecutar la comunicación procesa la información recibida.
         * Convierte el String de restpuesta en un DTORespuesta, si es correcto lo direcciona
         * a la venta principal, sino muestra mensaje de error según el detalle del error.
         */
        @Override
        protected void onPostExecute(SensorStateDto aVoid) {

        }
    }

    private class CambiarEstado extends AsyncTask<ReservaDto,Integer,Integer> {

        String jsonRespuestaEnString;

        @Override
        protected void onPreExecute(){

        }

        @Override
        protected void onProgressUpdate(Integer... values) {

        }

        @Override
        protected Integer doInBackground(ReservaDto... params) {

            /**
             * Convierte la información en DTOUsuario.
             */
            ReservaDto dto = (params[0]);
            HttpURLConnection conection = null;
            Boolean resutl = false;

            try {
                /**
                 * Define los datos de la conexión y sus caracteristicas, abre la conexión.
                 */
                URL url = new URL(ServicioRest.getInstancia().getUrl() + "parking/reserva");
                conection = (HttpURLConnection) url.openConnection();

                conection.setRequestMethod("POST");
                conection.setDoInput(true);
                conection.setDoOutput(true);
                conection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");

                /**
                 * Prepara el envío de datos mediante las clases OutputStream y BufferedWriter,
                 * en este caso el DTOUsuario.
                 */
                OutputStream outputStream = new BufferedOutputStream(conection.getOutputStream());
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream, "utf-8"));
                writer.write(dto.getJSON().toString());
                writer.flush();
                writer.close();

                /**
                 * Envía el DTOUsuario conectándose
                 */

                conection.setConnectTimeout(1000);
                conection.setReadTimeout(1000);

                try {
                    conection.connect();
                } catch (SocketTimeoutException e) {
//                    EstadoApp.getInstancia().setTieneConexion(false);
                    return null;
                } catch (ConnectException e) {
//                    EstadoApp.getInstancia().setTieneConexion(false);
                    return null;
                }

                /**
                 * Prepara la recepción de datos de la conexión mediante las clases InputStream
                 * y InputStreamReader.
                 */
                InputStream in = new BufferedInputStream(conection.getInputStream());
                InputStreamReader is = new InputStreamReader(in);

                /**
                 * Convierte los datos recibidos en un String y cierra la conexión
                 */
                StringBuilder sb = new StringBuilder();
                BufferedReader br = new BufferedReader(is);

                String read = br.readLine();

                while (read != null) {
                    sb.append(read);
                    read = br.readLine();
                }

                writer.close();
                br.close();
                conection.disconnect();

                jsonRespuestaEnString = sb.toString();

            } catch (MalformedURLException e) {
                Toast.makeText(getContext(), "Error en la URL", Toast.LENGTH_SHORT).show();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return 0;

        }

        /**
         * Después de terminar de ejecutar la comunicación procesa la información recibida.
         * Convierte el String de restpuesta en un DTORespuesta, si es correcto lo direcciona
         * a la venta principal, sino muestra mensaje de error según el detalle del error.
         */
        @Override
        protected void onPostExecute(Integer aVoid) {

        }
    }

}