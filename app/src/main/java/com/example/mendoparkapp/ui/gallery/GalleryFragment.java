package com.example.mendoparkapp.ui.gallery;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import com.example.mendoparkapp.MainActivity;
import com.example.mendoparkapp.R;
import com.example.mendoparkapp.model.Role;
import com.example.mendoparkapp.service.Preferencias;
import com.example.mendoparkapp.service.ReservationFactory;
import com.example.mendoparkapp.service.ServicioRest;
import com.example.mendoparkapp.ui.GlobalViewModel;
import com.example.mendoparkapp.ui.share.ShareFragment;
import com.example.mendoparkapp.ui.slideshow.SlideshowFragment;
import com.example.mendoparkapp.ui.tools.ToolsFragment;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class GalleryFragment extends Fragment implements OnMapReadyCallback{

    private GalleryViewModel galleryViewModel;
    private GlobalViewModel globalViewModel;
    private MainActivity mainActivity;
    private GalleryFragment myGalleryFragment;

    private Button btnGuardar;
    private View include;

    public List<ParkingAreaDto> parkingAreaDtos = null;
    public Map<String, List<Marker>> markersByParkingArea = new HashMap<>();
    public List<SensorStateDto> sensorStates = new ArrayList<>();

    public static class State{
        public static final String FREE = "LIBRE";
        public static final String BUSSY = "OCUPADO";
        public static final String RESERVED = "RESERVADO";
    }

    private int contador = 0;

    private class MarkerType{
        public static final String SENSOR = "SENSOR";
        public static final String PARKING_AREA = "PARKING_AREA";
    }

    public static class Sensor{
        public static final String s1 = "cny70Calle";
        public static final String s2 = "cny70A1";
        public static final Double la1 = -32.89647075924577D;
        public static final Double lo1 = -68.85342585239256D;
        public static final Double la2 = -32.896386443868295D;
        public static final Double lo2 = -68.85340258507061D;
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        galleryViewModel =
                ViewModelProviders.of(this).get(GalleryViewModel.class);
        globalViewModel =
                ViewModelProviders.of(this).get(GlobalViewModel.class);
        View root = inflater.inflate(R.layout.fragment_gallery, container, false);

        SupportMapFragment supportMapFragment =
                (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        supportMapFragment.getMapAsync(this);
//        include = root.findViewById(R.id.map_layout);
//        include.setVisibility(View.INVISIBLE);
        galleryViewModel.setLocation((EditText) root.findViewById(R.id.buscarEditT));
        btnGuardar = root.findViewById(R.id.buscarBtn);
        btnGuardar.setOnClickListener(buscarBtn);
        ejecutar();
        mainActivity = ((MainActivity)getActivity());
        myGalleryFragment = this;

        return root;
    }

    private  View.OnClickListener buscarBtn = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Consultar c =new Consultar();
            ConsultarLogin o = new ConsultarLogin();
            GeoPoint point = null;
            GoogleMap mMap = galleryViewModel.getGoogleMap().getValue();
            mMap.clear();

            try {
                point = o.execute(
                        galleryViewModel.getLocation().getValue().getText().toString()
                ).get();
            }
            catch(Exception e){
                Toast.makeText(
                        getContext(),
                        "ERROR",
                        Toast.LENGTH_SHORT
                ).show();
            }

            if(point == null){
                Toast.makeText(
                        getContext(),
                        "Localización no encontrada",
                        Toast.LENGTH_SHORT
                ).show();
            }else {

                LatLng sydney = new LatLng(point.getLatitude(), point.getLongitude());
                ConsultarAreas consultarAreas = new ConsultarAreas();

                try{
                    parkingAreaDtos = consultarAreas.execute(point).get();
                }
                catch (Exception e){
                    Toast.makeText(
                            getContext(),
                            "No se encontraron Áreas de estacionamiento cerca del lugar consultado",
                            Toast.LENGTH_SHORT
                    ).show();
                }

                if(parkingAreaDtos != null && parkingAreaDtos.size() > 0){

                    for (ParkingAreaDto parkingArea: parkingAreaDtos ) {
                        List<Marker> markers = new ArrayList<>();
                        String parkingAreaName = parkingArea.getName();

                        LatLng centerArea = new LatLng(parkingArea.getCenterPoint().getLatitude(), parkingArea.getCenterPoint().getLongitude());
                        mMap.addMarker(new MarkerOptions().position(centerArea).title(parkingArea.getName()).snippet(MarkerType.PARKING_AREA));

                        if(parkingArea.getParkingSpaces() != null && parkingArea.getParkingSpaces().size() > 0){
                            for (ParkingSpaceDto parkingSpace: parkingArea.getParkingSpaces() ) {
                                String parkingSpaceName = parkingSpace.getName();
                                if(parkingSpace.getSensor() != null){
                                    SensorDto sensor = parkingSpace.getSensor();
                                    LatLng sensorPoint = new LatLng(sensor.getLatitude(), sensor.getLongitude());
                                    MarkerOptions markerOptions = new MarkerOptions().position(sensorPoint)
                                            .title(sensor.getId()).snippet(parkingAreaName).anchor(0.5f,0.5f)
                                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.sensor));
                                    markers.add(mMap.addMarker(markerOptions));
                                }
                            }
                        }
                        if(parkingArea.getPolygonPoints()!=null && parkingArea.getPolygonPoints().size() > 0){
                            PolygonOptions options = new PolygonOptions();
                            for (int j = 0; j < parkingArea.getPolygonPoints().size() ; j++) {
                                PolygonPointsDto pointt = parkingArea.getPolygonPoints().get(j);
                                options.add(new LatLng(pointt.getLatitude(), pointt.getLongitude()));
                            }
                            options.strokeColor(0xFF00AA00)
                                    .fillColor(0x2200FFFF)
                                    .strokeWidth(2);
                            Polygon polygon = mMap.addPolygon(options);

                        }
                        markersByParkingArea.put(parkingAreaName,markers);
                    }

                }

                mMap.addMarker(new MarkerOptions().position(sydney).title("Punto Consultado"));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
                mMap.animateCamera(CameraUpdateFactory.zoomTo(17.0f));
                updateSensor();

//                if (c.estaDentro(point)) {
//    //        if(true){
//                    MarkerOptions mk = new MarkerOptions();
//
//                    mMap.addMarker(new MarkerOptions().position(sydney).title("Punto Consultado"));
//                    mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
//
//
//                    LatLng s1 = new LatLng(Sensor.la1, Sensor.lo1);
//                    mMap.addMarker(new MarkerOptions().position(s1).title(Sensor.s1)).setIcon(BitmapDescriptorFactory.fromResource(R.drawable.sensor));
//                    mMap.moveCamera(CameraUpdateFactory.newLatLng(s1));
//                    mMap.animateCamera(CameraUpdateFactory.zoomTo(17.0f));
//                    LatLng s2 = new LatLng(Sensor.la2, Sensor.lo2);
//                    mMap.addMarker(new MarkerOptions().position(s2).title(Sensor.s2)).setIcon(BitmapDescriptorFactory.fromResource(R.drawable.sensor));
//
//                } else {
//                    LatLng aa = new LatLng(point.getLatitude(), point.getLongitude());
//                    mMap.addMarker(new MarkerOptions().position(aa).title("Punto Consultado"));
//                    mMap.moveCamera(CameraUpdateFactory.newLatLng(aa));
//                    mMap.animateCamera(CameraUpdateFactory.zoomTo(17.0f));
//                }
            }
//            contador++;
//            if(contador > 5){
//                mMap.clear();
//            }

        }
    };

        private class ConsultarLogin extends AsyncTask<String,Integer,GeoPoint> {

            GeoPoint result;

            @Override
            protected void onPreExecute(){

            }

            @Override
            protected void onProgressUpdate(Integer... values) {

            }

            @SuppressLint("WrongThread")
            @Override
            protected GeoPoint doInBackground(String... params) {

                /**
                 * Convierte la información en DTOUsuario.
                 */
                String location = (params[0]);
                String resultAsResult = null;
                Double longitude = null;
                Double latitude = null;
                HttpURLConnection conection = null;

                location = location.trim();
                location = location.replaceAll(" ","%20");
                String urlString = Consultar.headUrl + location + Consultar.tailUrl;
                StringBuilder resultado = new StringBuilder();
                try {
                    // Crear un objeto de tipo URL
                    URL url = new URL(urlString);
                    conection = (HttpURLConnection) url.openConnection();

                    conection.setRequestMethod("GET");
                    conection.setDoInput(true);

                    conection.setConnectTimeout(1000);
                    conection.setReadTimeout(1000);

                    conection.connect();


                    /**
                     * Prepara la recepción de datos de la conexión mediante las clases InputStream
                     * y InputStreamReader.
                     */
                    InputStream in = new BufferedInputStream(conection.getInputStream());
                    InputStreamReader is = new InputStreamReader(in);

                    /**
                     * Convierte los datos recibidos en un String y cierra la conexión
                     */
                    StringBuilder sb = new StringBuilder();
                    BufferedReader br = new BufferedReader(is);

                    String read = br.readLine();

                    while(read!=null){
                        sb.append(read);
                        read = br.readLine();
                    }

                    br.close();
                    conection.disconnect();
                    resultAsResult = sb.toString();
                    if(resultAsResult != null && resultAsResult.length() > 2){
                        JSONArray responseBodyAsArray = null;
                        responseBodyAsArray = new JSONArray(new String(resultAsResult));
                        if(responseBodyAsArray.length() > 0){
                            JSONObject responseBodyAsJSON = new JSONObject(responseBodyAsArray.get(0).toString());
                            if(responseBodyAsJSON.has(Consultar.OpenStreetMap.LATITUDE) && responseBodyAsJSON.has(Consultar.OpenStreetMap.LONGITUDE)){
                                longitude = Double.parseDouble(responseBodyAsJSON.getString(Consultar.OpenStreetMap.LONGITUDE));
                                latitude = Double.parseDouble(responseBodyAsJSON.getString(Consultar.OpenStreetMap.LATITUDE));
                                result = new GeoPoint(latitude, longitude);
                            }
                        }
                    }


                }catch (Exception e){
                }
                return result;
            }

            /**
             * Después de terminar de ejecutar la comunicación procesa la información recibida.
             * Convierte el String de restpuesta en un DTORespuesta, si es correcto lo direcciona
             * a la venta principal, sino muestra mensaje de error según el detalle del error.
             */
            @Override
            protected void onPostExecute(GeoPoint aVoid) {

            }
    }

    private class ConsultarAreas extends AsyncTask<GeoPoint,Integer, List<ParkingAreaDto>> {

        List<ParkingAreaDto> result = new ArrayList<>();

        @Override
        protected void onPreExecute(){

        }

        @Override
        protected void onProgressUpdate(Integer... values) {

        }

        @SuppressLint("WrongThread")
        @Override
        protected List<ParkingAreaDto> doInBackground(GeoPoint... params) {

            /**
             * Convierte la información en DTOUsuario.
             */
            String resultAsResult = null;
            HttpURLConnection conection = null;

            GeoPoint requestPoint = params[0];

            String urlString = ServicioRest.getInstancia().getUrl() + "ParkingArea/parkingArea";
            StringBuilder resultado = new StringBuilder();
            try {
                // Crear un objeto de tipo URL
                URL url = new URL(urlString);
                conection = (HttpURLConnection) url.openConnection();

                conection.setRequestMethod("GET");
                conection.setDoInput(true);
                conection.setDoOutput(true);
                conection.setRequestProperty("Content-Type","application/json; charset=UTF-8");

                /**
                 * Prepara el envío de datos mediante las clases OutputStream y BufferedWriter,
                 * en este caso el DTOUsuario.
                 */
                OutputStream outputStream = new BufferedOutputStream(conection.getOutputStream());
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream,"utf-8"));
                writer.write(requestPoint.getJSON().toString());
                writer.flush();
                writer.close();

                conection.setConnectTimeout(1000);
                conection.setReadTimeout(1000);

                conection.connect();


                /**
                 * Prepara la recepción de datos de la conexión mediante las clases InputStream
                 * y InputStreamReader.
                 */
                InputStream in = new BufferedInputStream(conection.getInputStream());
                InputStreamReader is = new InputStreamReader(in);

                /**
                 * Convierte los datos recibidos en un String y cierra la conexión
                 */
                StringBuilder sb = new StringBuilder();
                BufferedReader br = new BufferedReader(is);

                String read = br.readLine();

                while(read!=null){
                    sb.append(read);
                    read = br.readLine();
                }

                br.close();
                conection.disconnect();
                resultAsResult = sb.toString();
                if(resultAsResult != null && resultAsResult.length() > 2){
                    JSONArray responseBodyAsArray = null;
                    responseBodyAsArray = new JSONArray(new String(resultAsResult));
                    if(responseBodyAsArray.length() > 0){
                        for (int j = 0; j < responseBodyAsArray.length(); j++) {
                            JSONObject responseBodyAsJSON = new JSONObject(responseBodyAsArray.get(j).toString());
                            ParkingAreaDto parkingAreaDto = new ParkingAreaDto();
                            if(responseBodyAsJSON.has("number")){
                                parkingAreaDto.setNumber(responseBodyAsJSON.getLong("number"));
                            }
                            if(responseBodyAsJSON.has("name")){
                                parkingAreaDto.setName(responseBodyAsJSON.getString("name"));
                            }
                            if(responseBodyAsJSON.has("color")){
                                parkingAreaDto.setColor(responseBodyAsJSON.getString("color"));
                            }
                            if(responseBodyAsJSON.has("centerPoint")){
                                PolygonPointsDto centerPoint = new PolygonPointsDto();
                                JSONObject jsonCenter = responseBodyAsJSON.getJSONObject("centerPoint");
                                if(jsonCenter.has("latitude")){
                                    centerPoint.setLatitude(jsonCenter.getDouble("latitude"));
                                }
                                if(jsonCenter.has("longitude")){
                                    centerPoint.setLongitude(jsonCenter.getDouble("longitude"));
                                }
                                parkingAreaDto.setCenterPoint(centerPoint);
                            }
                            if(responseBodyAsJSON.has("polygonPoints")){
                                JSONArray pointsArray = responseBodyAsJSON.getJSONArray("polygonPoints");
                                if(pointsArray!= null && pointsArray.length() > 0){
                                    List<PolygonPointsDto> polygonPointsDtos = new ArrayList<>();
                                    for (int i = 0; i < pointsArray.length(); i++) {
                                        JSONObject poinstAsJSON = new JSONObject(pointsArray.get(i).toString());
                                        PolygonPointsDto point = new PolygonPointsDto();
                                        if(poinstAsJSON.has("sequence")){
                                            point.setSequence(poinstAsJSON.getInt("sequence"));
                                        }
                                        if(poinstAsJSON.has("latitude")){
                                            point.setLatitude(poinstAsJSON.getDouble("latitude"));
                                        }
                                        if(poinstAsJSON.has("longitude")){
                                            point.setLongitude(poinstAsJSON.getDouble("longitude"));
                                        }
                                        polygonPointsDtos.add(point);
                                    }
                                    parkingAreaDto.setPolygonPoints(polygonPointsDtos);
                                }
                            }
                            if(responseBodyAsJSON.has("parkingSpaces")){
                                JSONArray parkingSpaces = responseBodyAsJSON.getJSONArray("parkingSpaces");
                                if(parkingSpaces != null && parkingSpaces.length() > 0){
                                    List<ParkingSpaceDto> parkingSpaceDtos = new ArrayList<>();
                                    for (int i=0; i<parkingSpaces.length(); i++) {
                                        JSONObject parkingSpace = parkingSpaces.getJSONObject(i);
                                        ParkingSpaceDto parkingSpaceDto = new ParkingSpaceDto();
                                        String name = parkingSpace.getString("name");
                                        if(name != null ) {
                                            parkingSpaceDto.setName(name);
                                        }
                                        if(parkingSpace.has("sensor")){
                                            JSONObject sensor = parkingSpace.getJSONObject("sensor");
                                            SensorDto sensorDto = new SensorDto();
                                            sensorDto.setId(sensor.getString("id"));
                                            sensorDto.setLatitude(sensor.getDouble("latitude"));
                                            sensorDto.setLongitude(sensor.getDouble("longitude"));
                                            parkingSpaceDto.setSensor(sensorDto);
                                        }
                                        parkingSpaceDtos.add(parkingSpaceDto);
                                    }
                                    parkingAreaDto.setParkingSpaces(parkingSpaceDtos);
                                }
                            }
                            result.add(parkingAreaDto);
                        }

                    }
                }


            }catch (Exception e){
                Log.d("HOLA",e.toString());
            }
            return result;
        }

        /**
         * Después de terminar de ejecutar la comunicación procesa la información recibida.
         * Convierte el String de restpuesta en un DTORespuesta, si es correcto lo direcciona
         * a la venta principal, sino muestra mensaje de error según el detalle del error.
         */
        @Override
        protected void onPostExecute(List<ParkingAreaDto> aVoid) {

        }
    }

    private class ConsultarEstadoSensor extends AsyncTask<String,Integer, SensorStateDto> {

        SensorStateDto result;

        @Override
        protected void onPreExecute(){

        }

        @Override
        protected void onProgressUpdate(Integer... values) {

        }

        @SuppressLint("WrongThread")
        @Override
        protected SensorStateDto doInBackground(String... params) {

            /**
             * Convierte la información en DTOUsuario.
             */
            String resultAsResult = null;
            HttpURLConnection conection = null;

            String sensorName = params[0];

            String urlString =ServicioRest.getInstancia().getUrl() + "sensorstate/"+sensorName;
            StringBuilder resultado = new StringBuilder();
            try {
                // Crear un objeto de tipo URL
                URL url = new URL(urlString);
                conection = (HttpURLConnection) url.openConnection();

                conection.setRequestMethod("GET");
                conection.setDoInput(true);

                conection.setConnectTimeout(1000);
                conection.setReadTimeout(1000);

                conection.connect();


                /**
                 * Prepara la recepción de datos de la conexión mediante las clases InputStream
                 * y InputStreamReader.
                 */
                InputStream in = new BufferedInputStream(conection.getInputStream());
                InputStreamReader is = new InputStreamReader(in);

                /**
                 * Convierte los datos recibidos en un String y cierra la conexión
                 */
                StringBuilder sb = new StringBuilder();
                BufferedReader br = new BufferedReader(is);

                String read = br.readLine();

                while(read!=null){
                    sb.append(read);
                    read = br.readLine();
                }

                br.close();
                conection.disconnect();
                resultAsResult = sb.toString();
                if(resultAsResult != null && resultAsResult.length() > 2){
                    JSONObject res = new JSONObject(resultAsResult);
                    result = new SensorStateDto();
                    if(res.has("name")){
                        result.setName(res.getString("name"));
                    }
                    if(res.has("state")){
                        result.setState(res.getString("state"));
                    }
                }else{
                    result = null;
                }


            }catch (Exception e){
            }
            return result;
        }

        /**
         * Después de terminar de ejecutar la comunicación procesa la información recibida.
         * Convierte el String de restpuesta en un DTORespuesta, si es correcto lo direcciona
         * a la venta principal, sino muestra mensaje de error según el detalle del error.
         */
        @Override
        protected void onPostExecute(SensorStateDto aVoid) {

        }
    }

    public void updateSensor(){
        Map<String, List<Marker>> listUpdated = new HashMap<>();
        Iterator it = markersByParkingArea.keySet().iterator();
        sensorStates = new ArrayList<>();
        while(it.hasNext()){
            String name = (String)it.next();
            List<Marker> markerList = markersByParkingArea.get(name);
            List<Marker> newListMarker = new ArrayList<>();
            if(markerList != null && markerList.size() > 0){
                for (Marker mk: markerList ) {
                    newListMarker.add(updateSensor(mk));
                }
            }
            listUpdated.put(name,newListMarker);
        }
        markersByParkingArea = listUpdated;
    }

    public Marker updateSensor(Marker marker){
        Marker result = marker;
        GoogleMap googleMap = galleryViewModel.getGoogleMap().getValue();
        String sensorName = marker.getTitle();
        ConsultarEstadoSensor consultarEstadoSensor = new ConsultarEstadoSensor();
        SensorStateDto dto = null;
        try {
            dto = consultarEstadoSensor.execute(sensorName).get();
        } catch (Exception ex) {
            Toast.makeText(
                    getContext(),
                    "ERROR al consultar estado de sensor",
                    Toast.LENGTH_SHORT
            ).show();
        }
        if (dto != null) {
            if(State.BUSSY.equals(dto.getState())){
                MarkerOptions markerOptions2 = new MarkerOptions().position(marker.getPosition())
                        .title(sensorName).snippet(marker.getSnippet()).anchor(0.5f,0.5f)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.sensor_ocupado));
                marker.remove();
                result = googleMap.addMarker(markerOptions2);
            }else if(State.FREE.equals(dto.getState())){
                MarkerOptions markerOptions2 = new MarkerOptions().position(marker.getPosition())
                        .title(sensorName).snippet(marker.getSnippet()).anchor(0.5f,0.5f)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.sensor_libre));
                marker.remove();
                result = googleMap.addMarker(markerOptions2);
            }else if(State.RESERVED.equals(dto.getState())){
                MarkerOptions markerOptions2 = new MarkerOptions().position(marker.getPosition())
                        .title(sensorName).snippet(marker.getSnippet()).anchor(0.5f,0.5f)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.sensor_reservado));
                marker.remove();
                result = googleMap.addMarker(markerOptions2);
            }
            sensorStates.add(dto);
        }
        return result;
    }



    @Override
    public void onMapReady(final GoogleMap googleMap) {
        boolean success = googleMap.setMapStyle(new MapStyleOptions(getResources()
                .getString(R.string.style_json)));
        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            public boolean onMarkerClick(Marker marker) {
                if(marker.getSnippet() != null && markersByParkingArea.containsKey(marker.getSnippet())) {
                    mainActivity.clearMenu2();
                    mainActivity.addPropertyToMenu2("Estacionamiento: ",marker.getSnippet());
                    ParkingAreaDto parkingArea = null;
                    ParkingSpaceDto parkingSpace = null;
                    SensorStateDto sensorState = null;
                    for (int i = 0; i < parkingAreaDtos.size() ; i++) {
                        if(parkingAreaDtos.get(i).getName().equals(marker.getSnippet())){
                            parkingArea = parkingAreaDtos.get(i);
                            for (int j = 0; j < parkingArea.getParkingSpaces().size(); j++) {
                                if(parkingArea.getParkingSpaces().get(j).getSensor().getId().equals(marker.getTitle())){
                                    parkingSpace = parkingArea.getParkingSpaces().get(j);
                                    break;
                                }
                            }
                            break;
                        }
                    }
                    for (int i = 0; i < sensorStates.size(); i++) {
                        if(sensorStates.get(i).getName().equals(marker.getTitle())){
                            sensorState = sensorStates.get(i);
                        }
                    }
                    if(parkingArea != null && parkingSpace != null && sensorState != null){
                        mainActivity.addPropertyToMenu2("Plaza: ",parkingSpace.getName());
                        mainActivity.addPropertyToMenu2("Precio: ",parkingArea.getPrecio());
                        mainActivity.addPropertyToMenu2("Disponibilidad: ",parkingArea.getDisponibilidad());
                        mainActivity.addPropertyToMenu2("Estado de plaza: ",sensorState.getState());

                    }
                    if(Preferencias.getInstanciaSinContexto().getRole() == Role.Type.CLIENT_ADMIN.getValue() ){
                        mainActivity.addPropertyToMenu2("Cambiar el estado de la plaza:","");
                        List<String> estados = Arrays.asList("LIBRE","OCUPADO");
                        for (int i = 0; i < estados.size(); i++) {
                            if(sensorState.getState().equals(estados.get(i))){
                                continue;
                            }
                            mainActivity.addBUttonToMenu2(getEventNameByState(estados.get(i)),cambiarEstado,parkingSpace.getName()+"|"+estados.get(i));
                        }

                    }else if(Preferencias.getInstanciaSinContexto().getRole() == Role.Type.USER.getValue()){
                        if(!sensorState.getState().equals("RESERVADO") || !sensorState.getState().equals("OCUPADO") ) {
                            mainActivity.addBUttonToMenu2("Reservar", reservarAccion, parkingSpace.getName() + "|" + State.RESERVED);
                        }
                        mainActivity.setParkingAreaReserva(parkingArea);
                        mainActivity.setParkingSpaceReserva(parkingSpace);
                    }else {
                        mainActivity.addBUttonToMenu2("Iniciar Sesión", iniciarSesion);
                    }
                    mainActivity.showMenu2();
                }
                return true;
            }
        });
        galleryViewModel.setGoogleMap(googleMap);
    }

    private String getEventNameByState(String state){
        if(state.equals("LIBRE")){
            return "Finalizar Estacionamiento";
        }else if(state.equals("OCUPADO")){
            return "Iniciar Estacionamiento";
        }else if(state.equals("RESERVADO")){
            return "Reservar";
        }else{
            return state;
        }
    }

    private void ejecutar(){
        final Handler handler= new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                updateSensor();//llamamos nuestro metodo
                handler.postDelayed(this,10000);//se ejecutara cada 10 segundos
            }
        },5000);//empezara a ejecutarse después de 5 milisegundos
    }


    View.OnClickListener reservarAccion = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Button b = (Button)v;
            String tag = (String) b.getTag();
            String parkingName = (tag.substring(0,tag.indexOf("|")));
            ReservationFactory.getInstance().crearReserva(parkingName,tag);
            Navigation.findNavController(getActivity(),R.id.nav_host_fragment).navigate(R.id.nav_slideshow);
        }
    };

    View.OnClickListener iniciarSesion = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Navigation.findNavController(getActivity(),R.id.nav_host_fragment).navigate(R.id.nav_share);
        }
    };

    View.OnClickListener cambiarEstado = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Button b = (Button)v;
            String stateMessage = (String) b.getTag();
            CambiarEstado cambiarEstado = new CambiarEstado();
            Integer res = null;
            try {
                res = cambiarEstado.execute(stateMessage).get();
                updateSensor();
            }
            catch(Exception e){
                Toast.makeText(
                        getContext(),
                        "ERROR al Registrar Usuario",
                        Toast.LENGTH_SHORT
                ).show();
            }
        }
    };

    private class CambiarEstado extends AsyncTask<String,Integer,Integer> {

        String jsonRespuestaEnString;

        @Override
        protected void onPreExecute(){

        }

        @Override
        protected void onProgressUpdate(Integer... values) {

        }

        @Override
        protected Integer doInBackground(String... params) {

            /**
             * Convierte la información en DTOUsuario.
             */
            String dto = (params[0]);
            HttpURLConnection conection = null;
            Boolean resutl = false;

            try {
                /**
                 * Define los datos de la conexión y sus caracteristicas, abre la conexión.
                 */
                URL url = new URL(ServicioRest.getInstancia().getUrl() + "sensorstate/fromUser");
                conection = (HttpURLConnection) url.openConnection();

                conection.setRequestMethod("POST");
                conection.setDoInput(true);
                conection.setDoOutput(true);
                conection.setRequestProperty("Content-Type", "text/plain; charset=UTF-8");

                /**
                 * Prepara el envío de datos mediante las clases OutputStream y BufferedWriter,
                 * en este caso el DTOUsuario.
                 */
                OutputStream outputStream = new BufferedOutputStream(conection.getOutputStream());
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream, "utf-8"));
                writer.write(dto);
                writer.flush();
                writer.close();

                /**
                 * Envía el DTOUsuario conectándose
                 */

                conection.setConnectTimeout(1000);
                conection.setReadTimeout(1000);

                try {
                    conection.connect();
                } catch (SocketTimeoutException e) {
//                    EstadoApp.getInstancia().setTieneConexion(false);
                    return null;
                } catch (ConnectException e) {
//                    EstadoApp.getInstancia().setTieneConexion(false);
                    return null;
                }

                /**
                 * Prepara la recepción de datos de la conexión mediante las clases InputStream
                 * y InputStreamReader.
                 */
                InputStream in = new BufferedInputStream(conection.getInputStream());
                InputStreamReader is = new InputStreamReader(in);

                /**
                 * Convierte los datos recibidos en un String y cierra la conexión
                 */
                StringBuilder sb = new StringBuilder();
                BufferedReader br = new BufferedReader(is);

                String read = br.readLine();

                while (read != null) {
                    sb.append(read);
                    read = br.readLine();
                }

                writer.close();
                br.close();
                conection.disconnect();

                jsonRespuestaEnString = sb.toString();

            } catch (MalformedURLException e) {
                Toast.makeText(getContext(), "Error en la URL", Toast.LENGTH_SHORT).show();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return 0;

        }

        /**
         * Después de terminar de ejecutar la comunicación procesa la información recibida.
         * Convierte el String de restpuesta en un DTORespuesta, si es correcto lo direcciona
         * a la venta principal, sino muestra mensaje de error según el detalle del error.
         */
        @Override
        protected void onPostExecute(Integer aVoid) {

        }
    }


}