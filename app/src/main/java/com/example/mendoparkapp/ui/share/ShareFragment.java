package com.example.mendoparkapp.ui.share;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.mendoparkapp.MainActivity;
import com.example.mendoparkapp.R;
import com.example.mendoparkapp.dto.usuario.DTOUsuario;
import com.example.mendoparkapp.model.Role;
import com.example.mendoparkapp.service.EstadoApp;
import com.example.mendoparkapp.service.Preferencias;
import com.example.mendoparkapp.service.ServicioRest;
import com.example.mendoparkapp.ui.gallery.Consultar;
import com.example.mendoparkapp.ui.gallery.GeoPoint;
import com.example.mendoparkapp.ui.send.UserDto;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.SocketTimeoutException;
import java.net.URL;

public class ShareFragment extends Fragment {

    public final class FieldType{
        public static final String INPUT = "input";
        public static final String PASS = "pass";
    }

    private ShareViewModel shareViewModel;

    private boolean registrado = false;
    private ShareFragment myShareFragment ;
    private View myView;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        shareViewModel =
                ViewModelProviders.of(this).get(ShareViewModel.class);
        View root = inflater.inflate(R.layout.fragment_share, container, false);
        myShareFragment = this;
        myView = root;
        updateView();
        return root;
    }

    public void updateView(){
        LinearLayout campos = myView.findViewById(R.id.contenido);
        campos.removeAllViews();
        TextView invisble = new TextView(getContext());
        invisble.setText("HAS");
        invisble.setPadding(0,150,0,150);
        invisble.setVisibility(View.INVISIBLE);
        if(!registrado){
            campos.addView(buildForm("Usuario: ", FieldType.INPUT, 350));
            campos.addView(buildForm("Contraseña: ", FieldType.PASS));
            campos.addView(buildButton("Iniciar Sesión", iniciarSesionAction));
            campos.addView(buildButton("Registrarse", registrarseViewAction));
        }else{
            campos.addView(buildForm("Nombre: ", FieldType.INPUT,350));
            campos.addView(buildForm("Apellido: ", FieldType.INPUT));
            campos.addView(buildForm("Nombre de Usuario: ", FieldType.INPUT));
            campos.addView(buildForm("Contraseña: ", FieldType.PASS));
            campos.addView(buildForm("Email: ", FieldType.INPUT));
            campos.addView(buildForm("Nro Documento: ", FieldType.INPUT));
            campos.addView(buildButton("Registrarse", registrarseAction));
        }
    }

    private LinearLayout buildButton(String name, View.OnClickListener action){
        LinearLayout linea = new LinearLayout(getContext());
        linea.setOrientation(LinearLayout.HORIZONTAL);

        Button btn = new Button(getContext());
        btn.setText(name);
        btn.setGravity(Gravity.CENTER_HORIZONTAL);
        btn.setOnClickListener(action);
        linea.addView(btn);
        return linea;
    }


    private LinearLayout buildForm(String name, String type) {
        return buildForm(name,type,5);
    }

    private LinearLayout buildForm(String name, String type, int top){

        LinearLayout linea = new LinearLayout(getContext());
        linea.setOrientation(LinearLayout.HORIZONTAL);

        TextView nombre = new TextView(getContext());
        nombre.setText(name);
        nombre.setPadding(10,top,0,0);
        linea.addView(nombre);
        EditText otro = new EditText(getContext());

        if(type.equals(FieldType.PASS)){
            otro.setInputType(129);
        }
        otro.setWidth(350);
        otro.setTag(name);
        linea.addView(otro);
        return linea;
    }

    private View.OnClickListener iniciarSesionAction = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DTOUsuario dto = new DTOUsuario();
            try {
                dto.setUsuario(((EditText) myView.findViewWithTag("Usuario: ")).getText().toString());
            }catch (Exception e){
                Toast.makeText(
                        getContext(),
                        "Por favor ingrese correctamente los datos",
                        Toast.LENGTH_SHORT
                ).show();
            }
            try {
                dto.setContrasenia(((EditText) myView.findViewWithTag("Contraseña: ")).getText().toString());
            }catch(Exception e){
                Toast.makeText(
                        getContext(),
                        "Por favor ingrese correctamente los datos",
                        Toast.LENGTH_SHORT
                ).show();
            }
            ConsultarSesion registrarUsuario = new ConsultarSesion();
            DTOUsuario res;
            try {
                res = registrarUsuario.execute(dto).get();
            }
            catch(Exception e){
                Toast.makeText(
                        getContext(),
                        "ERROR al Registrar Usuario",
                        Toast.LENGTH_SHORT
                ).show();
            }
        }
    };

    private View.OnClickListener registrarseAction = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            // se registra e inicia sesion
            UserDto dto = new UserDto();
            dto.setName(((EditText)myView.findViewWithTag("Nombre: ")).getText().toString());
            dto.setLastName(((EditText)myView.findViewWithTag("Apellido: ")).getText().toString());
            dto.setUserName(((EditText)myView.findViewWithTag("Nombre de Usuario: ")).getText().toString());
            dto.setPassword(((EditText)myView.findViewWithTag("Contraseña: ")).getText().toString());
            dto.setEmail(((EditText)myView.findViewWithTag("Email: ")).getText().toString());
            dto.setDocumentNumber(((EditText)myView.findViewWithTag("Nro Documento: ")).getText().toString());

            RegistrarUsuario registrarUsuario = new RegistrarUsuario();
            Boolean res;
            try {
                res = registrarUsuario.execute(dto).get();
            }
            catch(Exception e){
                Toast.makeText(
                        getContext(),
                        "ERROR al Registrar Usuario",
                        Toast.LENGTH_SHORT
                ).show();
            }

        }
    };

    private View.OnClickListener registrarseViewAction = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            registrado = true;
            updateView();
        }
    };


    private class RegistrarUsuario extends AsyncTask<UserDto,Integer,Boolean> {

        String jsonRespuestaEnString;

        @Override
        protected void onPreExecute(){

        }

        @Override
        protected void onProgressUpdate(Integer... values) {

        }

        @Override
        protected Boolean doInBackground(UserDto... params) {

            /**
             * Convierte la información en DTOUsuario.
             */
            UserDto dto = (params[0]);
            HttpURLConnection conection = null;
            Boolean resutl = false;

            try {
                /**
                 * Define los datos de la conexión y sus caracteristicas, abre la conexión.
                 */
                URL url = new URL(ServicioRest.getInstancia().getUrl()+"User/persist");
                conection = (HttpURLConnection) url.openConnection();

                conection.setRequestMethod("POST");
                conection.setDoInput(true);
                conection.setDoOutput(true);
                conection.setRequestProperty("Content-Type","application/json; charset=UTF-8");

                /**
                 * Prepara el envío de datos mediante las clases OutputStream y BufferedWriter,
                 * en este caso el DTOUsuario.
                 */
                OutputStream outputStream = new BufferedOutputStream(conection.getOutputStream());
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream,"utf-8"));
                writer.write(dto.getJSON().toString());
                writer.flush();
                writer.close();

                /**
                 * Envía el DTOUsuario conectándose
                 */

                conection.setConnectTimeout(1000);
                conection.setReadTimeout(1000);

                try{
                    conection.connect();
                }catch(SocketTimeoutException e){
                    EstadoApp.getInstancia().setTieneConexion(false);
                    return null;
                }catch (ConnectException e){
                    EstadoApp.getInstancia().setTieneConexion(false);
                    return null;
                }

                /**
                 * Prepara la recepción de datos de la conexión mediante las clases InputStream
                 * y InputStreamReader.
                 */
                InputStream in = new BufferedInputStream(conection.getInputStream());
                InputStreamReader is = new InputStreamReader(in);

                /**
                 * Convierte los datos recibidos en un String y cierra la conexión
                 */
                StringBuilder sb = new StringBuilder();
                BufferedReader br = new BufferedReader(is);

                String read = br.readLine();

                while(read!=null){
                    sb.append(read);
                    read = br.readLine();
                }

                writer.close();
                br.close();
                conection.disconnect();

                jsonRespuestaEnString = sb.toString();

            }
            catch(MalformedURLException e){
                Toast.makeText(getContext(), "Error en la URL", Toast.LENGTH_SHORT).show();
            }catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Preferencias.getInstanciaSinContexto().iniciarSesion();
            resutl = true;
            return resutl;
        }

        /**
         * Después de terminar de ejecutar la comunicación procesa la información recibida.
         * Convierte el String de restpuesta en un DTORespuesta, si es correcto lo direcciona
         * a la venta principal, sino muestra mensaje de error según el detalle del error.
         */
        @Override
        protected void onPostExecute(Boolean aVoid) {

            if(aVoid!= null &&  aVoid){
                Intent newForm = new Intent(getContext(), MainActivity.class);
                startActivity(newForm);
                Toast.makeText(getContext(), "Usuario Registrado correctamente... Disfrute de la aplicación!", Toast.LENGTH_LONG).show();
            }
        }
    }


    private class ConsultarSesion extends AsyncTask<DTOUsuario,Integer,DTOUsuario> {

        String jsonRespuestaEnString;

        @Override
        protected void onPreExecute(){

        }

        @Override
        protected void onProgressUpdate(Integer... values) {

        }

        @Override
        protected DTOUsuario doInBackground(DTOUsuario... params) {

            /**
             * Convierte la información en DTOUsuario.
             */
            DTOUsuario dto = (params[0]);
            HttpURLConnection conection = null;
            Boolean resutl = false;

            try {
                /**
                 * Define los datos de la conexión y sus caracteristicas, abre la conexión.
                 */
                URL url = new URL(ServicioRest.getInstancia().getUrl()+"session/");
                conection = (HttpURLConnection) url.openConnection();

                conection.setRequestMethod("POST");
                conection.setDoInput(true);
                conection.setDoOutput(true);
                conection.setRequestProperty("Content-Type","application/json; charset=UTF-8");

                /**
                 * Prepara el envío de datos mediante las clases OutputStream y BufferedWriter,
                 * en este caso el DTOUsuario.
                 */
                OutputStream outputStream = new BufferedOutputStream(conection.getOutputStream());
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream,"utf-8"));
                writer.write(dto.getJSON().toString());
                writer.flush();
                writer.close();

                /**
                 * Envía el DTOUsuario conectándose
                 */

                conection.setConnectTimeout(1000);
                conection.setReadTimeout(1000);

                try{
                    conection.connect();
                }catch(SocketTimeoutException e){
                    EstadoApp.getInstancia().setTieneConexion(false);
                    return null;
                }catch (ConnectException e){
                    EstadoApp.getInstancia().setTieneConexion(false);
                    return null;
                }

                /**
                 * Prepara la recepción de datos de la conexión mediante las clases InputStream
                 * y InputStreamReader.
                 */
                InputStream in = new BufferedInputStream(conection.getInputStream());
                InputStreamReader is = new InputStreamReader(in);

                /**
                 * Convierte los datos recibidos en un String y cierra la conexión
                 */
                StringBuilder sb = new StringBuilder();
                BufferedReader br = new BufferedReader(is);

                String read = br.readLine();

                while(read!=null){
                    sb.append(read);
                    read = br.readLine();
                }

                writer.close();
                br.close();
                conection.disconnect();

                jsonRespuestaEnString = sb.toString();

            }
            catch(MalformedURLException e){
                Toast.makeText(getContext(), "Error en la URL", Toast.LENGTH_SHORT).show();
            }catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Preferencias.getInstanciaSinContexto().iniciarSesion();
            return DTOUsuario.Factory.getDTORespuestaFromStringJson(jsonRespuestaEnString);
        }

        /**
         * Después de terminar de ejecutar la comunicación procesa la información recibida.
         * Convierte el String de restpuesta en un DTORespuesta, si es correcto lo direcciona
         * a la venta principal, sino muestra mensaje de error según el detalle del error.
         */
        @Override
        protected void onPostExecute(DTOUsuario aVoid) {

            if(aVoid!= null &&  aVoid.getSessionId()!= null){
                if(aVoid.getRole() != 0){
                    Preferencias.getInstanciaSinContexto().role(aVoid.getRole());
                }
                String roleMessage = "";
                if(aVoid.getRole() == Role.Type.USER.getValue()){
                    roleMessage = "usuario";
                }else if(aVoid.getRole() == Role.Type.CLIENT_ADMIN.getValue()){
                    roleMessage = "administrador";
                }
                Intent newForm = new Intent(getContext(), MainActivity.class);
                startActivity(newForm);
                Toast.makeText(getContext(), "Inicio de sesión correctamente como "+roleMessage+"... Disfrute de la aplicación!", Toast.LENGTH_LONG).show();
            }
        }
    }

}