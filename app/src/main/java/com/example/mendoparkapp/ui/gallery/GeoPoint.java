package com.example.mendoparkapp.ui.gallery;

import com.example.mendoparkapp.dto.ConvertidorJSON;

import org.json.JSONException;
import org.json.JSONObject;

public class GeoPoint implements ConvertidorJSON {

    private Double latitude;
    private Double longitude;

    public GeoPoint(Double latitude, Double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    @Override
    public JSONObject getJSON() throws JSONException {
        JSONObject json = new JSONObject();

        json.accumulate("latitude", this.latitude);
        json.accumulate("longitude", longitude);

        return json;
    }
}
