package com.example.mendoparkapp.service;


import com.example.mendoparkapp.model.ReservaDto;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class ReservationFactory {

    public boolean isEnviado() {
        return enviado;
    }

    public void setEnviado(boolean enviado) {
        this.enviado = enviado;
    }

    private boolean enviado = false;

    private static ReservationFactory instance = new ReservationFactory();

    private ReservaDto reservaNueva;
    private Map<String, ReservaDto> reservasHistoricas = new HashMap<>();

    private ReservationFactory(){}

    public static ReservationFactory getInstance(){
        if(instance == null){
            instance = new ReservationFactory();
        }
        return instance;
    }

    public void crearReserva(String parkingName, String eventoAccion){
        reservaNueva = new ReservaDto();
        reservaNueva.setParkingSpaceName(parkingName);
        reservaNueva.setEventoAccion(eventoAccion);
    }

    public ReservaDto getReservaNueva() {
        return reservaNueva;
    }

    public void setReservaNueva(ReservaDto reservaNueva) {
        this.reservaNueva = reservaNueva;
    }

    public void cerrarSesion(){
        this.reservasHistoricas = new HashMap<>();
    }

}
