package com.example.mendoparkapp.service;

import android.util.Log;

/**
 * Esta clase tiene como función tener la información importante del estado de la aplicación, estas
 * son:
 *
 *      ->  tieneConexion: este atributo sirve para saber si la aplicación tiene conexión ya sea
 *          a internet o al servidor para que trabaje ya sea conectado o de manera offline.
 */

public class EstadoApp {

    /* Singleton */
    private static EstadoApp instancia;

    private EstadoApp(){
        tiene_pedido_creado = Preferencias.getInstanciaSinContexto().hay_pedido();
    }

    public static EstadoApp getInstancia(){
        if(instancia == null){
            instancia = new EstadoApp();
        }
        return instancia;
    }

    /**
     * Atributos de la clase
     */

    private boolean tieneConexion;
    private boolean tiene_pedido_creado;

    public boolean isTieneConexion() {
        return tieneConexion;
    }

    public void setTieneConexion(boolean tieneConexion) {
        this.tieneConexion = tieneConexion;
        Log.d("EstadoApp", String.valueOf(this.tieneConexion));
    }

    public boolean isTiene_pedido_creado() {
        return tiene_pedido_creado;
    }

    public void setTiene_pedido_creado(boolean tiene_pedido_creado) {
        this.tiene_pedido_creado = tiene_pedido_creado;
    }

    public void agregar_Pedido(long p){
        setTiene_pedido_creado(true);
        Preferencias.getInstanciaSinContexto().agregar_Pedido();
        Preferencias.getInstanciaSinContexto().agregar_nro_Pedido(p);
    }

    public void quitar_Pedido(){
        setTiene_pedido_creado(false);
        Preferencias.getInstanciaSinContexto().quitar_Pedido();
    }

    public long get_nro_Pedido(){
        return Preferencias.getInstanciaSinContexto().get_nro_Pedido();
    }
}
