package com.example.mendoparkapp.service;

import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;

/**
 * Esta clase ServicioRest guarda la información de la dirección ip que se va a utilizar
 * para las llamadas REST, su función es obtener la ip correcta y darla cuando se la solicite.
 *
 * Es una clase SINGLETON ya que solo debe haber una clase para tal responsabilidad.
 */

public  class ServicioRest {

    public static final String METHOD_POST = "POST";
    public static final String METHOD_GET = "GET";

    private static ServicioRest instancia = new ServicioRest();

    private ServicioRest(){
        String ip = IndireccionServicioRest.obtenerInformacion();
        this.url = "http://"+ip+":8080/mendopark/rest/";
        Log.d("ServicioRest",url);
    }

    public static ServicioRest getInstancia(){
        if(instancia== null){
            instancia = new ServicioRest();
        }
        return instancia;
    }

    private String url ;

    public String getUrl() {
        return url;
    }

    public void actualizarURL(){
        String ip = IndireccionServicioRest.obtenerInformacion();
        url = "http://"+ip+":8080/mendopark/rest/";
    }

    private static  class IndireccionServicioRest{

        public static String obtenerInformacion(){
            Log.d("ServicioRest","APARECIOACAAAAAA");
            HttpURLConnection conexion = null;
            String rpta = "";
            try{

                URL url = new URL("https://raw.githubusercontent.com/gterickgt/primero/master/primero.txt");
                conexion = (HttpURLConnection) url.openConnection();

                conexion.setRequestMethod("GET");
                conexion.setDoInput(true);
                //conexion.setDoOutput(true);
                conexion.setRequestProperty("Content-Type","text/plain; charset=utf-8");


                conexion.setConnectTimeout(1000);
                conexion.setReadTimeout(1000);
                Log.d("ServicioRest","chiquito antes");
                try {
                    conexion.connect();
                }
                catch(SocketTimeoutException e){
                    Log.d("ServicioRest","SocketTimeoutException");
                    EstadoApp.getInstancia().setTieneConexion(false);
                    return null;
                }catch (ConnectException e){
                    Log.d("ServicioRest","ConnectException");
                    EstadoApp.getInstancia().setTieneConexion(false);
                    return null;
                }

                Log.d("ServicioRest","hizo conexion");
                InputStream in = new BufferedInputStream(conexion.getInputStream());
                InputStreamReader is = new InputStreamReader(in);

                StringBuilder sb = new StringBuilder();
                BufferedReader br = new BufferedReader(is);

                String read = br.readLine();

                while(read!=null){
                    sb.append(read);
                    read = br.readLine();
                }

                br.close();
                conexion.disconnect();

                rpta = sb.toString();
            }
            catch(MalformedURLException e){
                Log.d("ServicioRest","malformadola url en DTOProducto");
                EstadoApp.getInstancia().setTieneConexion(false);
                return null;
            }
            catch(IOException e){
                Log.d("ServicioRest",e.getMessage());
                EstadoApp.getInstancia().setTieneConexion(false);
                return null;
            }

            Log.d("ServicioRest",rpta);
            EstadoApp.getInstancia().setTieneConexion(true);
                return rpta;
        }

    }
}
