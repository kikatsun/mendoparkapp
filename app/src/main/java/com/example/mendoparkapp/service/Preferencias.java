package com.example.mendoparkapp.service;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.mendoparkapp.model.Role;

import static com.example.mendoparkapp.MainActivity.RoleUserType.ADMIN;

public class Preferencias {

    private static Preferencias ourInstance;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private Context context;

    private final String app = "Login";
    private final String inicioSesion = "InicioDeSesion";
    private final String ACTUALIZACION_CATEGORIAS = "Actualizacion_de_Categorias";
    private final String USUARIO = "Usuario";
    private final String HAY_PEDIDO = "Pedido";
    private final String NRO_PEDIDO = "nroPedido";
    private final String ROLE = "role"; // boolean

    public static Preferencias getInstance(Context context) {

        if(ourInstance==null){
            ourInstance = new Preferencias(context);
            return ourInstance;
        }
        else{
            return ourInstance;
        }
    }

    public static Preferencias getInstanciaSinContexto(){
        return ourInstance;
    }

    private Preferencias(Context activity){
        sharedPreferences = activity.getSharedPreferences(
                app,
                activity.getApplicationContext().MODE_PRIVATE
        );

    }

    public boolean consultarInicioDeSesion(){
        return  sharedPreferences.getBoolean(inicioSesion,false);
    }

    public void iniciarSesion(){
        editor = sharedPreferences.edit();
        editor.putBoolean(inicioSesion,true);
        editor.commit();
    }

    public void role(int roleId){
        editor = sharedPreferences.edit();
        editor.putInt(ROLE, roleId);
        editor.commit();
    }

    public int getRole(){
        return  sharedPreferences.getInt(ROLE,0);
    }

    public void cerrarSesion(){
        editor = sharedPreferences.edit();
        editor.putBoolean(inicioSesion,false);
        editor.remove(ROLE);
        editor.commit();
    }

    public int contultarActualizacionDeCategorias(){
        return sharedPreferences.getInt(ACTUALIZACION_CATEGORIAS,0);
    }

    public void actualizarActualizacionDeCategorias(int actualizacion){
        editor = sharedPreferences.edit();
        editor.putInt(ACTUALIZACION_CATEGORIAS,actualizacion);
        editor.commit();
    }

    public void setUsuario(String usuario){
        editor = sharedPreferences.edit();
        editor.putString(USUARIO,usuario);
        editor.commit();
    }

    public String getUsuario(){
        return sharedPreferences.getString(USUARIO,null);
    }

    public void agregar_Pedido(){
        editor = sharedPreferences.edit();
        editor.putBoolean(HAY_PEDIDO,true);
        editor.commit();
    }

    public void quitar_Pedido(){
        editor = sharedPreferences.edit();
        editor.putBoolean(HAY_PEDIDO,false);
        editor.commit();
    }

    public boolean hay_pedido(){
        return sharedPreferences.getBoolean(HAY_PEDIDO,false);
    }

    public void agregar_nro_Pedido(long nroPedido){
        editor = sharedPreferences.edit();
        editor.putLong(NRO_PEDIDO,nroPedido);
        editor.commit();
    }

    public long get_nro_Pedido(){
        return sharedPreferences.getLong(NRO_PEDIDO,0);
    }

}