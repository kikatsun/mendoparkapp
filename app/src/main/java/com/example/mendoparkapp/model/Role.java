package com.example.mendoparkapp.model;

public class Role {

    public enum Type{
        PUBLIC (0),
        USER (3),
        CLIENT_ADMIN (2);

        private final int type;

        private Type(int type){
            this.type = type;
        }

        public int getValue() {
            return this.type;
        }
    }

}
